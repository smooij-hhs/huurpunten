# Huurpunten

Deze applicatie berekent het totaal aantal huurpunten van een woning aan de hand van het aantal kamers en de eigenschappen van deze kamers.

Er zijn twee soorten kamers.

- RoomLiving: een woonkamer, het aantal punten is gelijk aan het oppervlak van de kamer,

- RoomBath: een badkamer, 20 punten als er een douche in zit, anders 10 punten.

Het puntensysteem wijzigt, er wordt een nieuw type kamer toegevoegd:

- RoomSleeping: het aantal punten is gelijk aan het aantal bedden maal 2.

Breid de applicatie uit met het nieuwe type kamer.
