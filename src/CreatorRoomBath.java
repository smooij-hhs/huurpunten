public class CreatorRoomBath implements CreatorRoom
{

	@Override
	public Room create()
	{
		System.out.print("Does the bathroom have a shower?: ");
		return new RoomBath(Main.SCANNER.next().equalsIgnoreCase("yes"));
	}

	@Override
	public String toString()
	{
		return "Bathoom";
	}
}