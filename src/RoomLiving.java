public class RoomLiving implements Room
{
	private final int length;
	private final int width;

	public RoomLiving(int length, int width)
	{
		this.length = length;
		this.width = width;
	}

	@Override
	public int getPoints()
	{
		return this.length * this.width;
	}

	@Override
	public String toString()
	{
		return "Livingroom (" + this.length + ", " + this.width + ") -> " + getPoints() + " points";
	}
}