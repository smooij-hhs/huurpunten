public interface CreatorRoom
{
	Room create();
}