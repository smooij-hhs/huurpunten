public class CreatorRoomLiving implements CreatorRoom
{

	@Override
	public Room create()
	{
		int length = Main.getNumber("length: ");
		int width = Main.getNumber("width: ");
		return new RoomLiving(length, width);
	}

	@Override
	public String toString()
	{
		return "Livingroom";
	}
}