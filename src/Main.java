import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main
{
	public static final Scanner SCANNER = new Scanner(System.in);
	public static final CreatorRoom[] CREATORS_ROOM = new CreatorRoom[]{new CreatorRoomLiving(), new CreatorRoomBath()};

	public static final List<Room> ROOMS = new ArrayList<>();
	public static int getNumber(String message)
	{
		System.out.print(message);
		return SCANNER.nextInt();

	}

	public static Room addRoom()
	{
		for (int index = 0; index < CREATORS_ROOM.length; index++)
		{
			System.out.print(index);
			System.out.println(": " + CREATORS_ROOM[index]);
		}
		int input = getNumber("New room type: ");
		return CREATORS_ROOM[input].create();
	}

	public static void removeRoom()
	{
		int number = getNumber("Delete room number: ");
		ROOMS.remove(number);
	}

	public static void overview()
	{
		System.out.println();
		System.out.println("Room overview:");
		for (int index = 0; index < ROOMS.size(); index++)
		{
			System.out.println(index + ": " + ROOMS.get(index));
		}
		System.out.println("Total points: " + calculate());
	}

	public static int calculate()
	{
		int result = 0;
		for (Room room : ROOMS)
		{
			result += room.getPoints();
		}
		return result;
	}

	public static void main(String[] args)
	{
		int input;
		do
		{
			overview();
			System.out.println();
			System.out.println("1: Add room");
			System.out.println("2: Remove room");
			System.out.println("0: Exit");
			input = getNumber("Your choice: ");
			switch (input)
			{
				case 1:
					ROOMS.add(addRoom());
					break;
				case 2:
					removeRoom();
					break;
			}
		}
		while (input != 0);
	}
}
