public class RoomBath implements Room
{
	private final boolean hasShower;

	public RoomBath(boolean hasShower)
	{
		this.hasShower = hasShower;
	}

	@Override
	public int getPoints()
	{
		return this.hasShower ? 20 : 10;
	}

	@Override
	public String toString()
	{
		return "Bathroom (" + (this.hasShower ? "with" : "without") + " shower) -> " + getPoints() + " points";
	}
}